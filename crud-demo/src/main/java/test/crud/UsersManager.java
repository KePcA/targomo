package test.crud;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class UsersManager {

	public static void main(String[] args) {
		
		String jdbcURL = "jdbc:mysql://localhost:3306/sampledb?serverTimezone=UTC";
		String dbUsername = "root";
		String dbPassword = "belgrade91";
		
		/*
		String fullname = "John Doe";
		String username = "johndoe";
		String email = "johndoe@gmail.com";
		String password = "123456789";
		*/
		
		try {
			Connection connection = DriverManager.getConnection(jdbcURL, dbUsername, dbPassword);
			
			/*
			 * INSERT
			 * 
			String sql = "INSERT INTO users (username, email, fullname, password)"
					+ " VALUES (?, ?, ?, ?)";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, username);
			statement.setString(2, email);
			statement.setString(3, fullname);
			statement.setString(4,  password);
			
			int rows = statement.executeUpdate();			
			if(rows > 0) {
				System.out.println("A new user has been inserted successfully");
			}
			*/
			
			/*
			 * SELECT
			 * 
			String sql = "SELECT * FROM users";
			Statement statement = connection.createStatement();
			ResultSet result = statement.executeQuery(sql);
			
			while (result.next()) {
				int userId = result.getInt("user_id");
				String username = result.getString("username");
				String fullname = result.getString("fullname");
				String email = result.getString("email");
				String password = result.getString("password");
				
				System.out.println(userId + ":" + username + ", " + fullname
						+ ", " + email + ", " + password);
			}
			*/
			
			
			/*
			 * UPDATE
			 *
			String sql = "UPDATE users SET password=?, fullname=?, email=? WHERE username=?";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, "javaguy");
			statement.setString(2, "John Jim");
			statement.setString(3, "john.jim@gmail.com");
			statement.setString(4, "johndoe");
			
			int rows = statement.executeUpdate();
			if(rows > 0) {
				System.out.println("The user's information has been updated.");
			}
			*/
			
			String sql = "DELETE FROM users WHERE username=?";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, "johndoe");
			
			int rows = statement.executeUpdate();
			if (rows > 0) {
				System.out.println("The user's information has been deleted.");
			}
			
			connection.close();
			
			
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

}
