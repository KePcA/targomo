package test.targomo;

import static org.junit.Assert.*;

import org.junit.Test;

public class StoreTest {
	
	@Test
	public void testDistanceToStore() {
		Store s1 = new Store("point 01", 13.364520827508, 52.53311053689);
		Store s2 = new Store("point 02", 13.348583789217, 52.49749976835);
		
		//Calculated at https://www.geodatasource.com/distance-calculator
		double actualDistanceInM = 4240;
		//Distance from s1 to s2 and from s2 to s1 (should be the same).
		double s1_to_s2 = s1.distanceToStore(s2);
		double s2_to_s1 = s2.distanceToStore(s1);
		
		//Distance from two stores should be equal no matter where we start.
		assertEquals(s1_to_s2, s2_to_s1, 0.001);
		//Compare the results to the actual distance on the map.
		assertEquals(actualDistanceInM, s1_to_s2, 1.0);
		assertEquals(actualDistanceInM, s2_to_s1, 1.0);
		System.out.println("Test test test ...");
	}
	
	/*
	@Test
	public void testTotalDistanceToStores() {
		fail("Not yet implemented.");
	}
	*/
}
