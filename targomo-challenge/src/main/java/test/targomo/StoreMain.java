package test.targomo;

import java.util.Arrays;

/**
 * Main method for calculating distances between the stores in the area.
 * Here we also sort the stores based on the total distance between each 
 * one and all other stores from the lowest to the highest.
 * @author Igor Klepic
 *
 */
public class StoreMain {
		
	public static void main(String[] args) {
		
		Store[] stores = {
				new Store("point 01", 13.364520827508, 52.53311053689),
				new Store("point 02", 13.348583789217, 52.49749976835),
				new Store("point 03", 13.32260893597, 52.496234335542),
				new Store("point 04", 13.399340217771, 52.52634868457),
				new Store("point 05", 13.475841247783, 52.509233081678),
				new Store("point 06", 13.327648640807, 52.486104681517),
				new Store("point 07", 13.41827673292, 52.500253429425),
				new Store("point 08", 13.478494056857, 52.542758172891),
				new Store("point 09", 13.27229044689, 52.49742389037),
				new Store("point 10", 13.423473566372, 52.574600538715),
		};
		
		calculateTotalDistances(stores);		
		Arrays.parallelSort(stores);
		printResults(stores);		
	}
	
	/**
	 * For each store it sums the distances to all the other stores
	 * and saves it in a totalDistance attribute of the store. 
	 * @param stores all the stores in the area.
	 */
	public static void calculateTotalDistances(Store[] stores) {
		for (Store store: stores) {
			store.totalDistanceToStores(stores);
		}
	}
	
	/**
	 * Simple method that prints the total distance to all the other
	 * stores from the each store.
	 * @param stores
	 */
	public static void printResults(Store[] stores) {
		for (Store store: stores) {
			System.out.println("Source " + store.getId() + " has a total distance of: " + store.getTotalDistance() + " m.");
		}
	}

}

