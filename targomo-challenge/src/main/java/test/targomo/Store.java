package test.targomo;

public class Store implements Comparable<Store>{
	
	private String id;
	private double latitude;
	private double longitude;
	/**
	 * Total distance to all the other stores in the area from this store.
	 */
	private double totalDistance;
	
	/**
	 * Constructor for this class.
	 * @param id the string id of the store.
	 * @param latitude of the store on the map.
	 * @param longitude of the store on the map.
	 */
	public Store(String id, double latitude, double longitude) {
		this.id = id;
		this.latitude = latitude;
		this.longitude = longitude;
		this.totalDistance = 0.0; //Not yet clculated.
	}
		
	public String getId() {
		return this.id;
	}
	
	public double getLatitude() {
		return this.latitude;
	}
	
	public double getLongitude() {
		return this.longitude;
	}
	
	public double getTotalDistance() {
		return totalDistance;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	/**
	 * Calculates the distance in meters between the two stores as 
	 * the distance between the two points on the map. 	
	 * @param s store to which we calculate the distance.
	 * @return the distance from this store to the store in the parameter.
	 */
	public double distanceToStore(Store s) {
		if ((this.latitude == s.latitude) && (this.longitude == s.longitude)) {
			return 0;
		}
		else {
			//Some math magic.
			double theta = this.longitude - s.longitude;
			double dist = Math.sin(Math.toRadians(this.latitude)) * Math.sin(Math.toRadians(s.latitude)) 
					+ Math.cos(Math.toRadians(this.latitude)) * Math.cos(Math.toRadians(s.latitude)) * Math.cos(Math.toRadians(theta));
			dist = Math.acos(dist);
			dist = Math.toDegrees(dist);
			//Result is in meters.
			dist = dist * 60 * 1.1515 * 1.609344 * 1000;
			return dist;
		}
	}
	
	/**
	 * Sums all the distances from this store to all the other stores.
	 * @param stores all the stores in the area from which we obtain distances.
	 */
	public void totalDistanceToStores(Store[] stores) {
		for (Store store: stores) {
			this.totalDistance += this.distanceToStore(store);
		}
	}
	
	/**
	 * Implements the compareTo method so we can compare two stores
	 * with the totalDistance attribute.
	 */
	public int compareTo(Store s) {
		return new Double(this.totalDistance).compareTo(s.totalDistance);
	}
	
	
}
